<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\Processor\LocalDeploy\Step\BuildCache\v5_0_0;

use XDev\Base\Processor\AStep;
use XDev\Utils\Shell;
/**
 * Class BuildCache
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class BuildCache extends AStep
{
    const MAX_ATTEMPTS = 200;
    const TIME_LIMIT = 200;

    public function getTitle()
    {
        return 'Building cache';
    }

    public function run()
    {
        Shell::setTranslateOutput(true);
        Shell::setTimeout(self::TIME_LIMIT);
        set_time_limit(self::TIME_LIMIT);

        $attempts = 0;

        $this->getOutput()->writeln('');

        $php_str = \XDev\Dev\Config\Main::getInstance()->getPhpPath();

        while (true) {

            $output = Shell::exec($php_str . ' admin.php');

            $attempts++;

            Shell::clearLastCommandOutput($this->getOutput());

            if (!preg_match('/Deploying\s+store/i', $output)) {
                break;
            }

            if ($attempts > self::MAX_ATTEMPTS) {
                throw new \Exception(aprintf('Max attempts %s exeeded', self::MAX_ATTEMPTS));
            }

        }

        Shell::setTranslateOutput(false);

    }
}
