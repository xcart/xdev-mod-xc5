<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\Processor\RemoteDeploy\v5_0_0;
use XDev\Module\XC5\Dev\EngineBased\XC5\Processor\LocalDeploy\v5_0_0\LocalDeploy;
/**
 * Class RemoteDeploy
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class RemoteDeploy extends \XDev\Dev\Processor\RemoteDeploy
{

    public function getLocalDeployIgnoredSteps()
    {
        return [
            LocalDeploy::S_CREATE_USERS,
        ];
    }
}
