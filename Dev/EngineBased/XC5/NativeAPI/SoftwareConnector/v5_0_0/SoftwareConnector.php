<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\NativeAPI\SoftwareConnector\v5_0_0;
 
/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SoftwareConnector implements \XDev\Dev\NativeAPI\ISoftwareConnector
{
    public function getConnectorCode()
    {
        return <<<EOT
require_once ('top.inc.php');
EOT;

    }
}
