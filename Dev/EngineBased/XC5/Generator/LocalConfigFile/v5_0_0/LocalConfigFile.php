<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Module\XC5\Dev\EngineBased\XC5\Generator\LocalConfigFile\v5_0_0;

use XDev\Utils\Filesystem;
use XDev\EM;

/**
 * Class Generator
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class LocalConfigFile extends \XDev\Module\XC5\Dev\EngineBased\XC5\Generator\LocalConfigFile\ALocalConfigFile
{
    public function getWebDir()
    {
        $web_dir = $this->getOption(self::PARAM_WEB_DIR);
        $web_dir = empty($web_dir) ? '' : '/' . $web_dir;

        return $web_dir;
    }

    public function generateFile()
    {

        $content = <<<END
; <?php /*

[database_details]
hostspec = "{$this->getOption(self::PARAM_DB_HOST)}"
socket   = "{$this->getOption(self::PARAM_DB_SOCKET)}"
port     = "{$this->getOption(self::PARAM_DB_PORT)}"
database = "{$this->getOption(self::PARAM_DB_NAME)}"
username = "{$this->getOption(self::PARAM_DB_USER)}"
password = "{$this->getOption(self::PARAM_DB_PASSWORD)}"

[host_details]
http_host   = "{$this->getOption(self::PARAM_HTTP_HOST)}"
https_host  = "{$this->getOption(self::PARAM_HTTP_HOST)}"
web_dir     = "{$this->getWebDir()}"

[performance]
developer_mode      = On
skins_cache         = off
compress_php_core   = off

[language]
default = en

END;
        return Filesystem::dumpFile(EM::get('Config')->getConfigLocalFilename(), $content);

    }
}
