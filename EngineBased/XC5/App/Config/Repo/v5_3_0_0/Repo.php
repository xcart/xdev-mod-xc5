<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\EngineBased\XC5\App\Config\Repo\v5_3_0_0;

/**
 * Class Repo
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Module\XC5\EngineBased\XC5\App\Config\Repo\v5_0_0\Repo
{

    protected function createGitignoreBuilder() {
// TODO: detect admin.php script thtough the API
        $builder = parent::createGitignoreBuilder();

        $builder
            ->section('xc5_allow')
                ->after('!/skins/**')
                ->addRule('!/vendor/**')
                ->addRule('!/xc5')
        ;

        return $builder;

    }
}
